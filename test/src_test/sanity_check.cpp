#include <gtest/gtest.h>
#include <stub.h>

TEST(check, sanity)
{
	EXPECT_EQ(2, add_two(1, 1));
}

TEST(check, insanity)
{
	EXPECT_NE(1, add_two(1,1));
}
